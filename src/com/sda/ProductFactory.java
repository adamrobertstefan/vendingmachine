package com.sda;

public class ProductFactory {

    public static Product[] buildProduct(ProductType productType, Integer nrOfProducts){
        Product[] result = new Product[nrOfProducts];
        for(int index = 0; index < nrOfProducts; index++){
            Product product = new Product(productType.getName(), productType.getPrice(), productType.getSize());
            result[index] = product;
        }


        return result;
    }
}
