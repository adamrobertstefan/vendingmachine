package com.sda;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class VendingMachine {

    private Map<String, Tray> stock;
    private InputOutPutService inputOutPutService;


    public VendingMachine() {
        this.stock = new TreeMap<>();
        this.inputOutPutService = new InputOutPutService();
        for (int index = 0; index < 10; index++) {

            /* String prefix = "00";
            if(index >=9){
            prefix = "0;
            }
             */
            String prefix = index >= 9 ? "0" : "00";
            stock.put(prefix + (index + 1), new Tray());
        }


    }

    public void loadProducts() {
        int index = 0;
        for (Map.Entry<String, Tray> entry : stock.entrySet()) {
            Tray tray = entry.getValue();
            tray.add(ProductFactory.buildProduct(ProductType.get(index++), 10));

        }
    }

    public void start() {
        do {
            inputOutPutService.displayMenu(stock);
            String userInput = inputOutPutService.getUserInput();
            process(userInput);

        } while (productsAvailable());
    }

    private boolean productsAvailable() {
        for (Tray tray : stock.values()) {
            if (tray.isNotEmpty()) {
                return true;
            }
        }
        return false;
    }

    private void process(String userInput) {
        //3. procesam input
        //	3.1 validare cod produs la pasul 2
        //		3.1.1 verificare existenta cod
        //		3.1.2 verificare stoc pt produs
        boolean validationSuccessful = isValid(userInput);

        if(!validationSuccessful){
            inputOutPutService.displayErrorForInput(userInput);
            return;
        }

    }

    private boolean isValid(String userInput) {
        Set<String> productCodes = stock.keySet();
        boolean codeExists = productCodes.contains(userInput);
        if (!codeExists) {
            return false;
        }
        Tray tray = stock.get(userInput);
        return tray.isNotEmpty();
    }
}

