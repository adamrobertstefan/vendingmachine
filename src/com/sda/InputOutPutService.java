package com.sda;

import java.util.Map;
import java.util.Scanner;

public class InputOutPutService {
    private Scanner scanner;

    public InputOutPutService(){
        scanner = new Scanner(System.in);
    }

    public void displayMenu(Map<String, Tray> stock) {
        System.out.println("\nWelcome! Please chose one of the following products: ");
        for(Map.Entry<String, Tray> entry : stock.entrySet()){
            String productCode = entry.getKey();
            Tray tray = entry.getValue();
            Product product = tray.peek();
            System.out.println(productCode + " "
                    + product.getName() + " "
                    + product.getSize() + " "
                    + product.getPrice() + " lei ");
        }
    }

    public String getUserInput(){
        System.out.print("Your answer: ");
        return scanner.nextLine();

    }

    public void displayErrorForInput(String userInput) {
        System.out.println("Sorry, " + userInput + " is not a valid product code! ");
    }
}
