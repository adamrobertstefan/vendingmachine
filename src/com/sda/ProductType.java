package com.sda;

public enum ProductType {

    COCA_COLA("coca cola", "500ml", 5),
    MARS("Mars", "50g", 3),
    DORNA("Dorna", "500ml", 2),
    FANTA("Fanta", "330ml", 4),
    REDBULL("Red Bull", "250ml", 10),
    SNICKERS("Snickers", "60", 6);

    private String name;
    private String size;
    private Integer price;

    ProductType(String name, String size, Integer price) {
        this.name = name;
        this.size = size;
        this.price = price;

    }

    public String getName() {
        return name;
    }

    public String getSize() {
        return size;
    }

    public Integer getPrice() {
        return price;
    }

    public static ProductType get(int number) {
        ProductType[] productTypes = ProductType.values();
        return productTypes[number % productTypes.length];

    }
}
